const mongoose = require('mongoose')/* requerimos a mongoose */
const url = 'mongodb://localhost/db_alumnos'/* creamos una constante q va a apuntar a nuestra base de datos */

/* configurar la conexion */
mongoose.connect(url,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
   /*  useFindAndModify: false,
    useCreateIndex: true */
})

/* definimos una constante db */
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Error al conectar MongoDB'))
db.once('open', function callback(){
    console.log("¡Conectado a MongoDB!")
})

module.exports = db /* exportamos */