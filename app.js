const express = require('express') /* requerir a express */
const app = express() /* crear una constante app invocando a express */

const db = require('./db')/* requerimos la conexion */

app.set('view engine', 'ejs')/* setear el motor de plantillas a ejs */

/* capturar datos de los inputs en formtao json para la edicion y creacion  */
app.use(express.urlencoded({extended:true}))
app.use(express.json())

app.use(express.static('public'))/* setear la carpeta public */

const alumnos = require('./routes/alumnos')/* requerimos el router */
app.use(alumnos)/* utilizamos alumnos */

app.get('/', (req,res) =>{
    res.send('hola mundo')
})

app.listen(3000, () => {
    console.log('¡Server UP! en http://localhost:3000')
})