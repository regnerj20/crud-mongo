const mongoose = require('mongoose')
const Schema = mongoose.Schema

const alumnoSchema = new Schema({
    nombre: String,
    edad: Number
},{versionKey:false})/* para q no se cree la clave al dar de alta */

module.exports = mongoose.model('alumnos', alumnoSchema)